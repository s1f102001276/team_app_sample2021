from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
import urllib
import json
import random

from .models import Reply

WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = 'P3yIaPlxCRyvqitxlIsUA8rx'
ACTION_HOW_ARE_YOU = 'HOW_ARE_YOU'

def index(request):
    positive_replies = Reply.objects.filter(response=Reply.POSITIVE)
    neutral_replies = Reply.objects.filter(response=Reply.NEUTRAL)
    negative_replies = Reply.objects.filter(response=Reply.NEGATIVE)

    context = {
        'positive_replies': positive_replies,
        'neutral_replies': neutral_replies,
        'negative_replies': negative_replies,

    }
    return render(request, 'index.html', context)

def clear(request):
    Reply.objects.all().delete()
    return redirect(index)

def announce(request):
    if request.method == 'POST':
        data = {
            'text': request.POST['message']
        }
        post_message(WEBHOOK_URL, data)

    return redirect(index)

@csrf_exempt
def echo(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'text': '<@{}> {}'.format(user_id, content.upper()),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def season(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> What is your favorite season :question:'.format(user_id)
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': 'I like',
                        'emoji': True
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'spring',
                                'emoji': True
                            },
                            'value': 'spring'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'summer',
                                'emoji': True
                            },
                            'value': 'summer'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'autumn',
                                'emoji': True
                            },
                            'value': 'autumn'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'winter',
                                'emoji': True
                            },
                            'value': 'winter'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'not decide',
                                'emoji': True
                            },
                            'value': 'not decide'
                        }
                    ],
                    'action_id': ACTION_HOW_ARE_YOU
                }
            }
        ],
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def reply(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    payload = json.loads(request.POST.get('payload'))
    print(payload)
    if payload.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    if payload['actions'][0]['action_id'] != ACTION_HOW_ARE_YOU:
        raise SuspiciousOperation('Invalid request.')
    
    user = payload['user']
    selected_value = payload['actions'][0]['selected_option']['value']
    response_url = payload['response_url']

    if selected_value == 'spring':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.POSITIVE)
        reply.save()
        text_list=['Let\'s see the cherry blossoms! :cherry_blossom:', 'Watch out for hay fever! :sneezing_face:', 'The weather is volatile in spring! :umbrella_with_rain_drops:',
                   'Lots of events! :dolls:', 'Congratulations on your admission and advancement! :female-student :']
        response = {
            'text': '<@{}> {}'.format(user['id'], text_list[random.randint(0,4)])
        }
    elif selected_value == 'summer':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
        reply.save()
        text_list=['Let\'s go to the beach or the pool! :umbrella_on_ground:', 'Let\'t go see the fireworks! :fireworks:',
                   'Let\'t go to the summer festival! :christina leung:', 'Let\'s go camping! :camping:']
        response = {
            'text': '<@{}> {}'.format(user['id'], text_list[random.randint(0,4)])
        }
    elif selected_value == 'autumn':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
        reply.save()
        text_list=['Let\'s see the maple! :maple_leaf:', 'Let\'s go grape picking!  :grapes:', 'Let\'s eat chestnuts!  :chestnut:',
                   'Let\'s enjoy Halloween!  :jack_o_lantern:', 'Let\'s see the moon!  :rice_scene:']
        response = {
            'text': '<@{}> {}'.format(user['id'], text_list[random.randint(0,4)])
        }
    elif selected_value == 'winter':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEGATIVE)
        reply.save()
        text_list=['Let\'s make a snowman!  :snowman:', 'Be careful not to catch a cold!  :sneezing_face:', 'Let\'s go skiing!  :skier:',
                   'Can you snowboard?  :snowboarder:', 'Did you believe in Santa Claus in your childhood?  :santa:']
        response = {
            'text': '<@{}> {}'.format(user['id'], text_list[random.randint(0,4)])
        }
    elif selected_value == 'not decide':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
        reply.save()
        response = {
            'text': '<@{}> I can not decide :worried:'.format(user['id'])
        }
    
    post_message(response_url, response)

    return JsonResponse({})

def post_message(url, data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(url, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()
